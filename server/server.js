const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5002;
require('./db')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const user = require("./routes/user");
const weather = require("./routes/weather");

app.use("/api/weather", weather)
app.use("/api/user", user)



app.listen(port, () => console.log(`Listening on port ${port}`));
