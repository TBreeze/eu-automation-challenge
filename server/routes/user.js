const router = require('express').Router()
const UserControler = require('../controllers/user')
const jwt = require('jsonwebtoken')
const authenticate = require('../middleware/authenticate')
router.post('/create/account', async (req, res) => {
    let user = await UserControler.create(req)
    if(user){
        res.send({success:true})

    }else{
        res.send({success:false})

    }
})
router.post('/login', async (req, res) => {
    var user = await UserControler.login({email: req.body.email, password: req.body.password})
    if(user){
        const jwtkey = 'dasdacxjiuojh32'
        token =  jwt.sign({email: req.body.email, id: user._id}, jwtkey)
        res.send({
            success: true,
            token: token
        })
    }else{
        res.send({success: false})
    }


})
router.get('/reauth', async (req, res) => {
    const authHeader = req.headers['x-auth-token']
    token = authHeader
        var decoded = jwt.decode(token)
        if(decoded){

        var user = await UserControler.findByEmail({email: decoded.email})
        if(user){
            res.send({
                success: true, 
                user: user,
            })
        }
    }else{
        res.send({success:false})
    }
    
})

router.post('/set/temperatures', authenticate, async (req, res) => {
    const authHeader = req.headers['x-auth-token']
    token = authHeader
    var decoded = await jwt.decode(token)
    var user = await UserControler.findByEmail({email: decoded.email})
    if(user){
        UserControler.setTemperatures(req.body.max, req.body.min, decoded.email)
        res.send({success:true})
    }
    
    
})
router.get('/get/temperatures', authenticate, async (req, res) => {
    const authHeader = req.headers['x-auth-token']
    token = authHeader
    var decoded = jwt.decode(token)
        var user = await UserControler.findByEmail({email: decoded.email})
        req.email = decoded.email
        if(user){
            user = await UserControler.findByEmail(req)
            res.send({success:true, max: user.idealTemperatureMax, min: user.idealTemperatureMin})
        }    
})
module.exports = router