const router = require('express').Router()
const WeatherControler = require('../controllers/weather')
const moment = require('moment')
const axios = require('axios')
router.post('/get/weather', async (req, res) => {

    date = moment().startOf('minute').unix()
    let currentWeather = await WeatherControler.findLastestByLocationAndDate(req.body.lat,req.body.long,date)
    if(currentWeather){
        res.send({success: true, weather: currentWeather})
    }else{
        await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${req.body.lat}&lon=${req.body.long}&appid=1bb5e54029c8ec0892f02aeeb698f5fc`).then(async res=>{
            let weather = {lat:req.body.lat,
            long:req.body.long,
            feelsLike: res.data.main.feels_like,
            humidity: res.data.main.humidity,
            pressure: res.data.main.pressure,
            temp: res.data.main.temp,
            tempMax: res.data.main.temp_max,
            tempMin: res.data.main.temp_min,
            locationName: res.data.name,
            date: moment().startOf('minute').unix()
        }
        currentWeather = await WeatherControler.create(weather)
        return 
    })
    res.send({success: true, weather: currentWeather})
    }
})

module.exports = router