const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Weather = new Schema ({
    lat: {type: Number, required: true},
    long: {type: Number, required: true},
    feelsLike: {type: Number, required: true},
    humidity: {type: Number, required: true},
    pressure: {type: Number, required: true},
    temp: {type: Number, required: true},
    tempMax: {type: Number, required: true},
    tempMin: {type: Number, required: true},
    locationName: {type: String, required: true},
    date: {type: Number, required: true}
})
module.exports = mongoose.model('Weather', Weather)