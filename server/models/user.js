const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema ({
    email: {type: String, required: true, unique: true},
    password: {type:String, required: true},
    favouriteLocations: {Type: Array},
    idealTemperatureMax: {Type: Number},
    idealTemperatureMin: {Type: Number}

})
module.exports = mongoose.model('User', User)