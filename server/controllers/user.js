const User = require('../models/user')
const bcrypt = require('bcryptjs')

exports.create = async function(req, res){
    var password = req.body.password
    var salt = bcrypt.genSaltSync(8)
    var hash = bcrypt.hashSync(password, salt)
    req.body.password = hash
    req.body.email  = req.body.email.toLowerCase()
    try {
        let user = await User.create(req.body)   
        return user         
    } catch (error) {
        return null
    }
}
exports.login = async function(req){
    var user = await User.findOne({email: req.email.toLowerCase()})
    if(user){
        var passwordCorrect = bcrypt.compareSync(req.password, user.password)
        if(passwordCorrect){
            return user
        }
    }
    return null
}
exports.findByEmail = async function(req){
    var user = await User.findOne({email: req.email.toLowerCase()})
    return user
}
exports.setTemperatures = async function(max,min, email){
    var user = await User.findOneAndUpdate({email: email},{'$set': {idealTemperatureMax: max, idealTemperatureMin: min}})
    return user
}
