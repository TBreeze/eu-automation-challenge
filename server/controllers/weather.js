const Weather = require('../models/weather')

exports.create = async function(currentWeather){
    let weather = await Weather.create(currentWeather)   
    return weather         
    
}
exports.findLastestByLocationAndDate = async function(lat,long,date){
    let weather = await Weather.findOne({lat: lat, long: long, date: date})   
    return weather           
}