-----About-----

This app displays the current weather for a specified location. When the site loads it finds the locaiton of the user via there ip and automatically loads the weather for that location.
The user can then enter specific coordinates and retrieve the weather for that area.

If a user creates an account they will then be able to enter there ideal temperature and the home weather screen will then display weather the weather is within there ideal range, may be colder or may be hotter.
This is calculated from the temperature range so a day may be able to be both hotter and colder than ideal (not a bug).

The weather api is called from the server at maximum once a minute for each coordinate to help avoid hitting the free limit of api calls. The database then stores a weather historicly for that location.
This should allow the app to work at scale better.

After each feature was integrated testing was performed to ensure each feature was working, aswell as previously implemented features.

Ideally a global state store like redux would have been used but i havent had much time available to do this aswell.

-----Preparing the site-----

Change database name on variable MONGO_DB server/db.js (optional)
Add database username on variable MONGO_USERNAME server/db.js 
Add database password on variable MONGO_PASSWORD server/db.js 

run "npm install" on the client folder
run "npm install" on the server folder





-----Creating a dev build-----

run "npm run dev" inside the server folder to create a development build

-----Creating a production build-----

run "npm run build" inside the client folder to create production clientside build and then run "node server.js" within the server folder
