import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import Home from './pages/home'
import NavBar from './components/navbar'
import Login from './pages/login'
import { useEffect, useState } from 'react';
import LoggedInRoute from './routes/loggedInRoute';
import LoggedOutRoute from './routes/loggedOutRoute';
import MyWeather from './pages/myWeather'
import axios from 'axios'

function App() {
  const [loggedIn, setLoggedIn] = useState(false)
  useEffect(() => {
    axios.defaults.headers.common['x-auth-token'] = localStorage.jwtToken
    axios.get('/api/user/reauth').then(res=>{
      if(res.data.success){
        localStorage.setItem('jwtToken', localStorage.jwtToken)
        axios.defaults.headers.common['x-auth-token'] = localStorage.jwtToken
        setLoggedIn(true)  
       }
    })  }, []);
  return (
    <div className="App" style={{marginTop:'60px'}}>
      <Router>
      <NavBar loggedIn={loggedIn} loggedOut={()=>{setLoggedIn(false);}}/>
        <Routes>
          <Route path="/" element={<Home loggedIn={loggedIn}/>}/>
          <Route path="/login" element={<LoggedOutRoute loggedIn={loggedIn}><Login loggedIn={()=>{setLoggedIn(true);}}/></LoggedOutRoute>}/>
          <Route path='/my-weather' element={<LoggedInRoute loggedIn={loggedIn}><MyWeather/></LoggedInRoute>}></Route>
          </Routes>
      </Router>
    </div>
  );
}

export default App;
          
