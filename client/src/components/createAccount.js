import React, { Component } from 'react';
import axios from 'axios';
import { Card, Form, Button } from 'react-bootstrap';
class CreateAccount extends Component {
    state = { 
        email: '',
        password: '',
        confirmPassword: '',
        errorCreate: '',
        successCreate: '',
     } 

    updateValue=(type, e)=>{
        this.setState({[type]:e.target.value})
    }
    createAccount=()=>{
        axios.post('/api/user/create/account',{email: this.state.email, password: this.state.password}).then((res)=>{
            if(res.data.success){
                this.setState({successCreate: 'Account Created. Please login.'})
            }else{
                this.setState({errorCreate: 'There has been an error. Please try again.'})
                
            }
        })
    }
    render() { 
        let createEnabled = false
        if(this.state.password == this.state.confirmPassword && this.state.email){
            createEnabled = true
        }
        return (
            <Card style={{width: '50%', margin: 'auto'}}>
                <Card.Header>
                    Create Account
                </Card.Header>
                <Card.Body>
                <Form.Group >
                        <Form.Label>Email</Form.Label>
                        <Form.Control placeholder="Email" type="email" value={this.state.long} onChange={(e)=>this.updateValue('email',e)} />
                    </Form.Group>
                <Form.Group >
                    <Form.Label>Password</Form.Label>
                    <Form.Control placeholder="Password" type="password" value={this.state.long} onChange={(e)=>this.updateValue('password',e)} />
                </Form.Group>
                <Form.Group >
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control placeholder="Password" type="password" value={this.state.long} onChange={(e)=>this.updateValue('confirmPassword',e)} />
                </Form.Group>
                <Button variant="primary" disabled={!createEnabled} onClick={this.createAccount}>
                        Create Account
                    </Button>
                {this.state.errorCreate && <p style={{color: 'red'}}>{this.state.errorCreate}</p>}
                {this.state.successCreate && <p style={{color: 'green'}}>{this.state.successCreate}</p>}
                </Card.Body>
            </Card>
        );
    }
}
 
export default CreateAccount;