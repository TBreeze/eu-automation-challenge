import React, { Component } from 'react';
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from "react-router-dom"
import axios from 'axios';
class NavBar extends Component {
    state = { 
        expanded: false
     } 
    toggle=()=>{
        this.setState({expanded: !this.state.expanded})
    }
    close = ()=>{
        this.state.expanded && this.toggle()
    }
    logout=()=>{
        this.props.loggedOut()
        localStorage.removeItem('jwtToken')
            delete axios.defaults.headers.common['x-auth-token']
    }
    render() { 
        return (
            <Navbar expanded={this.state.expanded} onToggle={this.toggle} expand='md' bg="dark" variant="dark" fixed='top'>
                    <Navbar.Brand as={Link} to="/"></Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse on >
                        <Nav style={{width: '100%'}} className="justify-content-end">
                            <Nav.Link onClick={this.close} as={Link} to="/">Home</Nav.Link>
                                
                            {this.props.loggedIn && <Nav.Link as={Link} to="/my-weather" onClick={()=>{this.close();}}>My Weather</Nav.Link>}

{this.props.loggedIn &&<Nav.Link onClick={()=>{this.close();this.logout()}}>Logout</Nav.Link>}

                                
                           { !this.props.loggedIn &&<Nav.Link onClick={this.close} as={Link} to="/login">Login</Nav.Link>

                                
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
        );
    }
}
 
export default NavBar;