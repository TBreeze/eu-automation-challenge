import React, { Component } from 'react';
import axios from 'axios';
import { Card, Form, Button } from 'react-bootstrap';
class Login extends Component {
    state = { 
        email: '',
        password: '',
        loginMessage: ''
     } 
    updateValue=(type, e)=>{
        this.setState({[type]:e.target.value})
    }
    login=()=>{
        axios.post('/api/user/login',{email: this.state.email, password: this.state.password}).then((res)=>{
            if(res.data.success){
                this.setState({loginMessage: 'Success'})
                localStorage.setItem('jwtToken', res.data.token)
                axios.defaults.headers.common['x-auth-token'] = res.data.token
                this.props.loggedIn()
            }else{
                this.setState({loginMessage: 'Unable to login'})
                
            }
        })
    }
    render() { 
        return (
            <Card style={{width: '50%', margin: 'auto'}}>
                <Card.Header>
                    Login
                </Card.Header>
                <Card.Body>
                <Form.Group >
                        <Form.Label>Email</Form.Label>
                        <Form.Control placeholder="Email" type="email" value={this.state.long} onChange={(e)=>this.updateValue('email',e)} />
                    </Form.Group>
                <Form.Group >
                    <Form.Label>Password</Form.Label>
                    <Form.Control placeholder="Password" type="password" value={this.state.long} onChange={(e)=>this.updateValue('password',e)} />
                </Form.Group>
                <Button variant="primary" onClick={this.login}>
                        Login
                    </Button>
                {this.state.loginMessage && <p>{this.state.login}</p>}

                </Card.Body>
            </Card>
        );
    }
}
 
export default Login;