import {Navigate} from "react-router-dom"
import React, { Component } from 'react';
class LoggedInRoute extends Component {
   
    render() {
        if(!this.props.loggedIn){
            return (
                <div>{this.props.children}</div>
            )
        }
        else{
            return (
                <Navigate  to={{pathname: '/'}}/>
            )
        }    
    }
}

 
export default LoggedInRoute;
