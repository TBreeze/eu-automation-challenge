import axios from 'axios';
import React, { Component } from 'react';
import { Form, Button,Row, Col } from 'react-bootstrap';
import './home.css'
class Home extends Component {
    state = { 
        lat: 0,
        long: 0,
        latRetrieved: 0,
        longRetrieved: 0,
        feelsLike: 0,
        humidity: 0,
        pressure: 0,
        temp: 0,
        tempMax: 0,
        tempMin: 0,
        locationName: '',
        idealMax: null,
        idealMin: null
        
     } 
    componentDidMount(){
        delete axios.defaults.headers.common['x-auth-token']
        axios.get('https://geolocation-db.com/json/').then((res)=>{
            this.setState({lat:res.data.latitude, long: res.data.longitude},()=>{
                this.getWeather()
            })
        })
        axios.defaults.headers.common['x-auth-token'] = localStorage.jwtToken
        axios.get('/api/user/get/temperatures').then(res=>{
            if(res.data.success){
                this.setState({idealMax: res.data.max,idealMin: res.data.min})

            }
        })
    
    }
    updateValue=(type, e)=>{
        this.setState({[type]:e.target.value})
    }
    getWeather=()=>{
        axios.post(`/api/weather/get/weather`,{lat:this.state.lat,long:this.state.long}).then(
            res=>{
                this.setState({
                    latRetrieved:res.data.weather.lat,
                    longRetrieved:res.data.weather.long,
                    feelsLike: res.data.weather.feelsLike,
                    humidity: res.data.weather.humidity,
                    pressure: res.data.weather.pressure,
                    temp: res.data.weather.temp,
                    tempMax: res.data.weather.tempMax,
                    tempMin: res.data.weather.tempMin,
                    locationName: res.data.weather.locationName
                })
            }
        )
    }
    render() { 
        let getDisabled = (this.state.lat === this.state.latRetrieved) && (this.state.long === this.state.longRetrieved)
        let idealWeather = false
        let colder = false
        let warmer = false
        if(this.props.loggedIn){
            if(this.state.tempMin >= parseInt(this.state.idealMin) && this.state.tempMax <= parseInt(this.state.idealMax)){
                idealWeather = true
            }else{
                if(this.state.tempMin < parseInt(this.state.idealMin)){
                colder = true  
            }
            if(this.state.tempMax > parseInt(this.state.idealMax)){
                warmer = true  
            }
        }
        
    }
        return (
            <div>
                <h1>Weather App</h1>
                {idealWeather && <div className='animate'><p style={{color: 'green'}}>Today is your ideal temperature</p><img style={{width: '50px'}} alt="" src={require('../images/happy.png')}/></div>}
                {colder && <div className='animate'><p style={{color: 'blue'}}>Today may get colder than you would like</p><img style={{width: '50px'}} alt="" src={require('../images/cold.png')}/></div>}
                {warmer && <div className='animate'><p style={{color: 'red'}}>Today may get warmer than you may like</p> <img style={{width: '50px'}} alt="" src={require('../images/warm.png')}/></div>}

                <h4>Location</h4>
                <p>Name: {this.state.locationName}</p>
                <p>Latitude: {this.state.latRetrieved} Longitude: {this.state.longRetrieved}</p>
                <h4>Temperature</h4>
                <p>Feels Like: {this.state.feelsLike}</p>
                <p>Pressure: {this.state.pressure}</p>
                <p>Current: {this.state.temp}</p>
                <p>Min: {this.state.tempMin} Max: {this.state.tempMax} </p>

                <p>Humidity: {this.state.humidity}</p>

                <Form>
                    <Row>
                        <Col>
                        <Form.Group className="mb-3" >
                        <Form.Label>Latitude</Form.Label>
                        <Form.Control placeholder="Latitude" type="number" value={this.state.lat} onChange={(e)=>this.updateValue('lat',e)}  />
                        
                    </Form.Group>
                        </Col>
                    
                    <Col>
                    <Form.Group className="mb-3" >
                        <Form.Label>Longitude</Form.Label>
                        <Form.Control placeholder="Longitude" type="number" value={this.state.long} onChange={(e)=>this.updateValue('long',e)} />
                    </Form.Group>
                    </Col>
                    
                    
                    </Row>
                    <Button variant="primary" disabled={getDisabled} onClick={this.getWeather}>
                        Get Weather
                    </Button>

                   
                </Form>


            </div>
        );
    }
}
 
export default Home;