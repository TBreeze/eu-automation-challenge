import axios from 'axios';
import React, { Component } from 'react';
import { Card, Form ,Button} from 'react-bootstrap';
class MyWeather extends Component {
    state = { 
        min:0,
        max:0
     }
     componentDidMount(){
        axios.get('/api/user/get/temperatures').then(res=>{
            if(res.data.success){
                this.setState({max: res.data.max,min: res.data.min})

            }
        })

     }
     setTemperatures=()=>{
        axios.post('/api/user/set/temperatures', {max: this.state.max, min: this.state.min})
     }
     updateValue=(type, e)=>{
        this.setState({[type]: e.target.value})
     }
    render() { 
        return (<div>
            <Card style={{width: '50%', margin: 'auto'}}>
                <Card.Header>Ideal Temperatures</Card.Header>
                <Card.Body>
                <Form.Group >
                    <Form.Label>Maximum</Form.Label>
                    <Form.Control placeholder="Maximum" type="number" value={this.state.max} onChange={(e)=>this.updateValue('max',e)} />
                </Form.Group>
                <Form.Group >
                    <Form.Label>Minimum</Form.Label>
                    <Form.Control placeholder="Minimum" type="number" value={this.state.min} onChange={(e)=>this.updateValue('min',e)} />
                </Form.Group>
                <Button variant="primary"  onClick={this.setTemperatures}>
                        Set Temperatures
                    </Button>
                </Card.Body>
            </Card>
        </div>);
    }
}
 
export default MyWeather;