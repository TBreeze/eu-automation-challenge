import React, { Component } from 'react';
import Login from '../components/login'
import CreateAccount from '../components/createAccount'

class LoginPage extends Component {
    state = { 
        email: '',
        password: '',
        confirmPassword: '',
        errorCreate: '',
        successCreate: '',
        loginMessage: ''
     } 

    render() { 
        return (
            <div >
                <div style={{marginTop: '100px'}}>
                <Login />
                </div>
                <div style={{marginTop: '100px'}}>
                    <CreateAccount loggedIn={this.props.loggedIn} style={{marginTop: '100px'}}/>
                </div>
            
            </div>
        );
    }
}
 
export default LoginPage;